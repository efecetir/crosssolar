using CrossSolar.Domain;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrossSolar.Repository
{
    public class DayAnalyticsRepository : OneDayElectricityModel, IDayAnalyticsRepository
    {
        private readonly CrossSolarDbContext _dbContext;
        public DayAnalyticsRepository(CrossSolarDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public Task<List<OneDayElectricityModel>> GetAnalyticsHistoryDataAsync(int panelId)
        {

            var OneDayElectricitys = _dbContext.OneHourElectricitys.GroupBy(x => x.DateTime.Date).Select(y => new OneDayElectricityModel
            {
                Average = y.Average(z => z.KiloWatt),
                Sum = y.Sum(z => z.KiloWatt),
                Maximum = y.Max(z => z.KiloWatt),
                Minimum = y.Min(z => z.KiloWatt),
                DateTime = y.FirstOrDefault().DateTime.Date
            });

            //var oneDayElectricityListModel = new OneDayElectricityListModel()
            //{
            //    OneDayElectricitys = await OneDayElectricitys.ToListAsync()
            //};

            return OneDayElectricitys.ToListAsync();
        }

        //public Task<List<OneDayElectricityModel>> GetAnalyticsHistoryDataAsync(int panelId)
        //{
        //    var oneHourElectricity = _dbContext.OneHourElectricitys
        //       .Where(panel => panel.Id == panelId)
        //       .OrderByDescending(panel => panel.DateTime)
        //       .GroupBy(panel => new
        //       {
        //           panel.DateTime.Year,
        //           panel.DateTime.Month,
        //           panel.DateTime.Day
        //       });
        //    //day[sum, min, max, average]
        //    var history = oneHourElectricity.Select(
        //        s => new OneDayElectricityModel
        //        {
        //            Sum = s.Sum(panel => panel.KiloWatt),
        //            Minimum = s.Min(panel => panel.KiloWatt),
        //            Maximum = s.Max(panel => panel.KiloWatt),
        //            Average = s.Average(panel => panel.KiloWatt),
        //            DateTime = new DateTime(s.Key.Year, s.Key.Month, s.Key.Day, 0, 0, 0)
        //        }).ToListAsync();
        //    return history;
        //}

        public Task<OneDayElectricityModel> GetAsync(string id)
        {
            throw new System.NotImplementedException();
        }

        public Task InsertAsync(OneDayElectricityModel entity)
        {
            throw new System.NotImplementedException();
        }

        public IQueryable<OneDayElectricityModel> Query()
        {
            throw new System.NotImplementedException();
        }

        public Task UpdateAsync(OneDayElectricityModel entity)
        {
            throw new System.NotImplementedException();
        }
    }
}