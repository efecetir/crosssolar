﻿using CrossSolar.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrossSolar.Models
{
    public class OneDayElectricityListModel
    {
        public IEnumerable<OneDayElectricityModel> OneDayElectricityList { get; set; }

    }
}
