using CrossSolar.Domain;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrossSolar.Repository
{
    public class AnalyticsRepository : GenericRepository<OneHourElectricity>, IAnalyticsRepository
    {
        public AnalyticsRepository(CrossSolarDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public Task<List<OneHourElectricity>> GetPanelById(int panelId)
        {
            return _dbContext.OneHourElectricitys.Where(x => x.PanelId == panelId.ToString()).ToListAsync();
        }
    }
}