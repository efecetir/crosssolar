﻿using System.Collections.Generic;
using System.Linq;

namespace CrossSolar.Models
{
    public class OneHourElectricityListModel
    {
        public List<OneHourElectricityModel> OneHourElectricitys { get; set; }
    }
}