﻿using CrossSolar.Controllers;
using CrossSolar.Domain;
using CrossSolar.Models;
using CrossSolar.Repository;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace CrossSolar.Tests.Controller
{
    public class AnalyticsControllerTests
    {
        private readonly AnalyticsController _analyticsController;
        private readonly Mock<IAnalyticsRepository> _analyticsRepositoryMock = new Mock<IAnalyticsRepository>();
        private readonly Mock<IPanelRepository> _panelRepositoryMock = new Mock<IPanelRepository>();
        private readonly Mock<IDayAnalyticsRepository> _dayAnalyticsRepositoryMock = new Mock<IDayAnalyticsRepository>();
        public AnalyticsControllerTests()
        {
            _analyticsController = new AnalyticsController(_analyticsRepositoryMock.Object, _panelRepositoryMock.Object, _dayAnalyticsRepositoryMock.Object);
        }

        [Fact]
        public async Task Post_ShouldInsertAnalytics()
        {
            var oneHourElectricityModel = new OneHourElectricityModel
            {
                DateTime = DateTime.Now,
                KiloWatt = 5357
            };

            // Arrange

            // Act
            var result = await _analyticsController.Post(4, oneHourElectricityModel);

            // Assert
            Assert.NotNull(result);

            var createdResult = result as CreatedResult;
            Assert.NotNull(createdResult);
            Assert.Equal(201, createdResult.StatusCode);
        }

        [Fact]
        public void Get_ShouldGetAnalyticsData()
        {
            // Arrange

            // Act
            var result = _analyticsController.Get(5).Result;
            var objectResult = result as ObjectResult;
            // Assert
            Assert.NotNull(result);
            Assert.NotNull(objectResult);
            Assert.Equal(200, objectResult.StatusCode);
        }

        [Fact]
        public void Get_ShouldDayResultsData()
        {
            var result = _analyticsController.DayResults(5).Result;
            var objectResult = result as ObjectResult;
            // Assert
            Assert.NotNull(result);
            Assert.NotNull(objectResult);
            Assert.Equal(200, objectResult.StatusCode);
        }
    }
}
