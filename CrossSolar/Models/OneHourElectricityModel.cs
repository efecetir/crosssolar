﻿using System;

namespace CrossSolar.Models
{
    public class OneHourElectricityModel
    {
        public int Id { get; set; }

        public long KiloWatt { get; set; }

        public long Watt
        {
            get
            {
                return KiloWatt * 1000;
            }
        }
        public DateTime DateTime { get; set; }
    }
}