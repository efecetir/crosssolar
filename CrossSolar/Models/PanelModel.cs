﻿using System.ComponentModel.DataAnnotations;

namespace CrossSolar.Models
{
    public class PanelModel
    {
        public int Id { get; set; }

        [Required]
        [Range(-90, 90)]
        //[RegularExpression(@"^\d+(\.\d{6})$")]
        [RegularExpression(@"^([-+]?)([0-9]{1,2}([.,][0-9]{1,6}))?$", ErrorMessage = "Invalid input: It must contains maximum 6 decimal places.")]
        public double Latitude { get; set; }

        [Range(-180, 180)]
        [RegularExpression(@"^([-+]?)([0-9]{1,3}([.,][0-9]{1,6}))?$", ErrorMessage = "Invalid input: It must contains maximum 6 decimal places.")]
        public double Longitude { get; set; }

        [Required]
        [StringLength(16, MinimumLength = 16)]
        public string Serial { get; set; }

        public string Brand { get; set; }
    }
}