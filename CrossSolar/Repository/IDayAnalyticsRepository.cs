﻿using CrossSolar.Domain;
using CrossSolar.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CrossSolar.Repository
{
    public interface IDayAnalyticsRepository : IGenericRepository<OneDayElectricityModel>
    {
        Task<List<OneDayElectricityModel>> GetAnalyticsHistoryDataAsync(int panelId);
    }
}